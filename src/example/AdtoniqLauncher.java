package example;


/** This simple class implements a singleton pattern to provide access to Adtoniq services
 * and cache Adtoniq parameters across all pages
 * @author David
 *
 */
public class AdtoniqLauncher {
	/** This points to the single instance of adtoniq. Customize this call here to pass in
	 * your API key and fully qualified domain name of the front end of your website.
	 */
	public final static Adtoniq adtoniq = new Adtoniq("b4304032-f8c2-465f-aa1e-eda6e9e6fa64", "localhost:58201");
}
